var express = require('express');
app = express();

const fs = require('fs');

const port = process.env.PORT || 5000;

var bodyParser = require('body-parser');

app.use(express.static(__dirname + '/pages'));

app.use(bodyParser.json());
app.set('view engine', 'ejs');
app.set('views', './views')

app.get('/', function(req, res){
    res.render('home', { message: 'Hello, from index', current: 'home' });
});

app.get('/contacts', function(req, res){
    res.render('contacts', { message: 'Hello, from contacts', current: 'contacts' });
});

app.post('/admin/{id}', function(req, res){
    
    let x = fs.readFileSync(__dirname + '/pages/admin.html', 'utf-8');
    // res.redirect('admin.html');
    res.send(x);
});

app.post('/javascript', function(req, res){
    res.send('javascript');
});

app.listen(port);